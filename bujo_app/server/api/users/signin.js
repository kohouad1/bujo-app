const express = require("express");
const router = express.Router();
const upload = require("multer")();
const bcrypt = require("bcrypt");
const Auth = require("../../auth");
const User = require("../../db/models/User");

router.post("/signin", upload.none(), function (req, res) {
  if (!req.body.username || !req.body.password) {
    return res.status(422).send({
      message: "Username and password parameters are required."
    });
  }
  let username = req.body.username;
  let password = req.body.password;
  User.find({ username }, (err, docs) => {
    if (err) {
      return res.status(400).send({
        message: err
      });
    }
    if (docs.length === 0) {
      return res.status(404).send({
        message: "The username does not exist."
      });
    }
    let user = docs[0];
    bcrypt.compare(password, user.password, (err, result) => {
      if (!result) {
        return res.status(401).send({
          message: "The password is incorrect."
        });
      }
      Auth.signInUser(req, res, user);
      res.status(200).send({
        message: "Successfully signed in."
      });
    });
  });
});

module.exports = router;
