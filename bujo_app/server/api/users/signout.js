const express = require("express");
const router = express.Router();

router.post("/signout", function (req, res) {
  res.cookie("access_token", null, { expires: new Date(Date.now() - 3600000) });
  res.cookie("refresh_token", null, { expires: new Date(Date.now() - 3600000) });

  req.cookies["access_token"] = null;
  req.cookies["refresh_token"] = null;
  res.status(200).send({
    message: "Successfully signed out."
  });
});

module.exports = router;
