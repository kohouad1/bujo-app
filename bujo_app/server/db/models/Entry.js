const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

const EntrySchema = mongoose.Schema({
  userId: {
    type: String,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  type: {
    type: String,
    enum: ["TASK", "TASK_COMPLETED", "EVENT", "NOTE"],
    default: "TASK"
  },
  signifier: {
    type: String,
    enum: ["", "IMPORTANT", "INSPIRATION"],
    default: ""
  },
  canceled: {
    type: Boolean,
    required: true
  },
  dateDailyLog: {
    type: Date,
    required: true
  },
  dateMonthlyLog: {
    type: Date,
    default: null
  }
});

EntrySchema.add({
  parentEntry: {
    type: String,
    default: null
  },
  childEntries: [EntrySchema]
});

module.exports = mongoose.model("Entry", EntrySchema);
