require("dotenv").config();
const mongoose = require("mongoose");

mongoose.connect(process.env.DB_URI,
         { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on("error", (err) => console.log("Error connecting to the database.", err));
mongoose.connection.once("open", () => console.log("Successfully connected to the database."));

module.exports = mongoose;
