# BuJo App - Bullet Journal Web Application
BuJo App is a web application that is based on the popular [Bullet Journal Method](https://bulletjournal.com/).

## Installation

First, in order to download and run the project, you need to have the following installed on your machine:

- [Git](https://git-scm.com/) - needed only if you want to clone the project (instead of cloning it you can download the zipped version).
- [Node.js](https://nodejs.org/en/) - download the LTS version.
- [npm](https://www.npmjs.com/) - recommended to install together with Node.js.
- [MongoDB database](https://www.mongodb.com/download-center/community) - install as a Service with the option "Run service as Network Service user" selected.

Now you can proceed to setup and launch the project:
1. Clone the project using `git clone git@gitlab.fel.cvut.cz:kohouad1/bujo-app.git` from your terminal, or download the zipped version of the project.
2. Open the .env file, located in the `/server` directory, and update the key-value pairs:
   - `DB_URI` is a URI the application uses to connect to the database; if you are using the local database, you can leave it unchanged.
   - `JWT_ACCESS_SECRET` and `JWT_REFRESH_SECRET` are keys used for generating authorization tokens; it can be any non-empty string, however it is recommended to use long strings containing random characters, and both keys should be different.
3. Setup the client side:
   - In your terminal, make sure you are in the `/bujo_app/client` directory.
   - Install the dependencies for the client side using `npm install`.
   - After the installation is finished, build the client side using `npm run build-dev` (or `npm run build` for production environment). A `build` directory in the `/bujo_app/server` directory will be created and will be served by the server.
4. Setup and run the server side:
   - In your terminal, make sure you are in the `/bujo_app/server` directory (`cd ../server`)
   - Install the dependencies for the server side using `npm install`.
   - Run the server (application) using `npm run dev` (or `run dev start` for production environment).
5. You can now access the application from a browser at `http://localhost:5000/`.